## -*- Mode: Makefile; -*-                                             
##
## © Copyright Jamie A. Jennings 2019, 2021.
## © Copyright IBM Corporation 2016, 2017.
## LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
## AUTHOR: Jamie A. Jennings

ROSIE = "$(shell command -v rosie)"

ROSIE_DYLIB_NAME=rosie
ifeq ($(PLATFORM), macosx)
ROSIE_DYLIB=lib$(ROSIE_DYLIB_NAME).dylib
else
ROSIE_DYLIB=lib$(ROSIE_DYLIB_NAME).so
endif

REPORTED_PLATFORM=$(shell (uname -o || uname -s) 2> /dev/null)
ifeq ($(REPORTED_PLATFORM), Darwin)
PLATFORM=macosx
else ifeq ($(REPORTED_PLATFORM), GNU/Linux)
PLATFORM=linux
else
PLATFORM=none
endif

default: notarget

 # Some Linux distros will fail on $(shell command -v true)
true=$(shell /bin/sh -c 'command -v true')
python3=$(shell command -v python3 || command -v python || command -v true 2>/dev/null)
python_version=$(shell $(python3) --version 2>&1)

notarget:
	echo $(python_version)
	@echo 'No target specified.'
	@echo ''
	@echo 'Use "make <python>" to build a python source distribution (in ./dist), where'
	@echo '<python> is python3 or python (which uses your system default python version).'
	@echo ''
	@echo 'Or "make test" to run the unit tests.  (Does not depend on build.)'

python2:
	@echo "Python2 is no longer supported." && exit -1

python3:
	python3 setup.py sdist

python:
	python setup.py sdist

test:
	@echo Testing with $(shell $(python3) --version 2>&1); \
	export LD_LIBRARY_PATH='/usr/lib:/usr/local/lib'; \
	echo "Library path to search for librosie.so is $$LD_LIBRARY_PATH"; \
	cd test; \
	$(python3) -c 'import cffi'; \
	if [ $$? -eq 0 ]; then \
		$(python3) test.py; \
		$(python3) examplerosie.py; \
		$(python3) examplere.py; \
		$(python3) testRPLlibrary.py; \
	else echo "cffi not found -- skipping tests"; exit -1; \
	fi

clean:
	$(RM) rosie.pyc 
	$(RM) "__pycache/*.pyc"

echo:
	@echo "HOME= $(HOME)"
	@echo "PLAT= $(PLAT)"
	@echo "CC= $(CC)"
	@echo "CFLAGS= $(CFLAGS)"
	@echo "LDFLAGS= $(LDFLAGS)"
	@echo "LIBS= $(LIBS)"
	@echo "RM= $(RM)"


## Targets that do not create files
.PHONY: default clean echo test
