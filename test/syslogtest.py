#  -*- Mode: Python; -*-                                                   
#  -*- coding: utf-8; -*-
# 
#  syslogtest.py
# 
#  © Copyright Jamie A. Jennings 2021.
#  LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
#  AUTHOR: Jamie A. Jennings

import time
import sys, os, json
assert( sys.version_info.major == 3 )

# We want to test the rosie package in the parent directory, not any
# rosie package that happens to be visible to Python.  There must be
# an idiomatic way to do this, because this way is a hack.
sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))
import rosie

def match_syslog_file(file_handle, posonly=False):
    match_count = 0
    non_match_count = 0

    for line in file_handle:
        match = pattern.match(line, posonly=posonly)
        if match is None:
            non_match_count += 1
        else:
            match_count += 1

    print("Processed {} lines: {} matched, {} failed to match".format(
        non_match_count + match_count, match_count, non_match_count))

def match_using_internal_interface(filename):
    total, match_count, non_match_count = e._engine.matchfile(pattern._internal_rplx, b'byte', filename, b'/dev/null')
    print("Processed {} lines: {} matched, {} failed to match".format(
        non_match_count + match_count, match_count, non_match_count))

# -----------------------------------------------------------------------------

if __name__ == '__main__':
    librosiedir = None
    if len(sys.argv) == 1:
        librosiedir = rosie.librosie_system()
        print("Loading librosie from system library path")
    elif len(sys.argv) == 2:
        librosiedir = sys.argv[1]
        print("Loading librosie from path given on command line: " + librosiedir)
    else:
        sys.exit("Error: spurious command-line parameters (one or none are expected)")
    rosie.load(librosiedir)
    testdir = os.path.dirname(os.path.abspath(__file__))
    os.chdir(testdir)                 # To find files needed for testing

    e = rosie.engine()
    e.loadfile('syslog-2018-09-05.rpl')
    pattern = e.compile('syslog')
    filename = b'syslog100k'

    # Warm up any OS file cache by processing the entire file once
    # before we time anything:
    print("Warming up...")
    match_using_internal_interface(filename)

    print("Matching {} using Python iterator and getting data fields...".format(filename))
    syslog_datafile = open(filename, 'r')
    t1_data = time.process_time_ns()
    match_syslog_file(syslog_datafile)
    delta_t_data = time.process_time_ns() - t1_data
    syslog_datafile.close()
    print("Matching time (ms): {:>12,}".format(int(delta_t_data/1000)))

    print("Matching {} using Python iterator and NOT getting data fields...".format(filename))
    syslog_datafile = open(filename, 'r')
    t1_pos = time.process_time_ns()
    match_syslog_file(syslog_datafile, True)
    delta_t_pos = time.process_time_ns() - t1_pos
    syslog_datafile.close()
    print("Matching time (ms): {:>12,}".format(int(delta_t_pos/1000)))
    
    print()
    print("Using posonly took about {:2.1f}% less time".format(
        (delta_t_data - delta_t_pos) * 100 / delta_t_data))
    print()

    print("Matching {} using HIDDEN internal rosie matchfile() ...".format(filename))
    t1_internal = time.process_time_ns()
    match_using_internal_interface(filename)
    delta_t_internal = time.process_time_ns() - t1_internal
    print("Matching time (ms): {:>12,}".format(int(delta_t_internal/1000)))
    print()
    print("Internal matchfile() is roughly {:3.1f}x faster than Python (with data fields)".format(delta_t_data/delta_t_internal))
    print("Internal matchfile() is roughly {:3.1f}x faster than Python (posonly)".format(delta_t_pos/delta_t_internal))
    print()
    print("NOTE: Results from matchfile() are being directed to /dev/null, which boosts performance")
    
